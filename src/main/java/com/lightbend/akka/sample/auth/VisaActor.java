package com.lightbend.akka.sample.auth;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.lightbend.akka.sample.network.VisaNetworkActor;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

public class VisaActor extends UntypedAbstractActor {

    private final ActorRef visaNetworkActor;

    public static Props props() {
        return Props.create(VisaActor.class, () -> new VisaActor());
    }

    public VisaActor() {
        visaNetworkActor = getContext().getSystem().actorOf(VisaNetworkActor.props(), "visaNetworkActor");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        System.out.println(getSelf().toString());
        System.out.println("Sending :" + message);
        Timeout timeout = new Timeout(Duration.create(5, "seconds"));
        Future<Object> future = Patterns.ask(visaNetworkActor, message, timeout);
        String result = (String) Await.result(future, timeout.duration());
        System.out.println("Response :" + result);
    }
}
