package com.lightbend.akka.sample.auth;

import akka.actor.AbstractActor;
import akka.actor.ActorSystem;
import akka.japi.pf.ReceiveBuilder;

public class PaymentActor extends AbstractActor {

    public PaymentActor () {

    }

    @Override
    public Receive createReceive() {
        return new ReceiveBuilder().match(MastercardActor.class, mastercardActor -> {
            System.out.println("");
        }).build();
    }
}
