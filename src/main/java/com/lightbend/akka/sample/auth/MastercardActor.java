package com.lightbend.akka.sample.auth;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.pattern.Patterns;
import akka.routing.RoundRobinPool;
import akka.routing.RouterConfig;
import akka.util.Timeout;
import com.lightbend.akka.sample.network.MastercardNetworkActor;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

public class MastercardActor extends UntypedAbstractActor {

    private final ActorRef mastercardNetworkActor;

    public static Props props() {
        return Props.create(MastercardActor.class, () -> new MastercardActor());
    }

    public MastercardActor() {
        //mastercardNetworkActor = getContext().getSystem().actorOf(MastercardNetworkActor.props(), "mastercardNetworkActor");
        mastercardNetworkActor = getContext().actorOf(new RoundRobinPool(10).props(Props.create(MastercardNetworkActor.class)),
                "mastercardNetworkActor");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        System.out.println(getSelf().toString());
        System.out.println("Sending :" + message);
        Timeout timeout = new Timeout(Duration.create(5, "seconds"));
        Future<Object> future = Patterns.ask(mastercardNetworkActor, message, timeout);
        String result = (String) Await.result(future, timeout.duration());
        System.out.println("Response :" + result + " " + System.currentTimeMillis());

    }

}
