package com.lightbend.akka.sample;

import com.lightbend.akka.sample.receiver.ActiveMQListener;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class AkkaQuickstart {
  private static String url = "tcp://localhost:61616";


  public static void main(String[] args) throws JMSException {
    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

    Connection connection = connectionFactory.createConnection();
    connection.start();
    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    Destination destination = session.createQueue("TEST.FOO");
    MessageConsumer consumer = session.createConsumer(destination);
    consumer.setMessageListener(new ActiveMQListener());
  }

}
