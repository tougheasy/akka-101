package com.lightbend.akka.sample.receiver;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.gson.Gson;
import com.lightbend.akka.sample.auth.MastercardActor;
import com.lightbend.akka.sample.auth.VisaActor;
import com.lightbend.akka.sample.domain.AuthRequest;

import javax.jms.*;

public class ActiveMQListener implements MessageListener{

    private final ActorRef mastercardActor;
    private final ActorRef visaActor;
    private final ActorSystem system;

    public ActiveMQListener() {
        system = ActorSystem.create("paymentakka");
        visaActor = system.actorOf(VisaActor.props(), "visaActor");
        mastercardActor = system.actorOf(MastercardActor.props(), "mastercardActor");
    }

    @Override
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                System.out.println("Received message" + textMessage.getText() + "'");

                Gson gson = new Gson();
                try {
                    AuthRequest authRequest = gson.fromJson(textMessage.getText(), AuthRequest.class);

                    if (authRequest.getCardType().equalsIgnoreCase("Visa")) {
                        visaActor.tell(authRequest, ActorRef.noSender());
                    } else if (authRequest.getCardType().equalsIgnoreCase("Mastercard")) {
                        mastercardActor.tell(authRequest, ActorRef.noSender());

                    }
                } catch (Exception ex) {
                    System.out.println("Not a JSON");
                }

            } else {
                System.out.println("Not a TextMessage");
            }
        } catch (JMSException e) {
            System.out.println("Caught:" + e);
            e.printStackTrace();
        }
    }
}
