package com.lightbend.akka.sample.network;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

public class VisaNetworkActor extends UntypedAbstractActor {

    public static Props props() {
        return Props.create(VisaNetworkActor.class, () -> new VisaNetworkActor());
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        System.out.println(getSelf().toString() + ": APPROVED");
        getSender().tell("APPROVED", ActorRef.noSender());
    }
}
