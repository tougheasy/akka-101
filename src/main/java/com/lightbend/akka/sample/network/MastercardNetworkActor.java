package com.lightbend.akka.sample.network;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

public class MastercardNetworkActor extends UntypedAbstractActor {

    public static Props props() {
        return Props.create(MastercardNetworkActor.class, () -> new MastercardNetworkActor());
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        System.out.println(getSelf().toString() + ": APPROVED");
        Thread.sleep(30);
        getSender().tell("APPROVED", ActorRef.noSender());
    }
}
